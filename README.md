SlackPorts (Sport)
====================

``sport`` is a Slackware Port system based upon the idea of BSD Ports. It models
itself after the BSD Handbook's instructions for using the Ports Collection.

It has no dependencies apart from a normal Slackware install.

It is compatible with all official arches of Slackware, including ARM.


http://slackermedia.info/sport
http://gitlab.com/slackport/sport